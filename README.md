# Delivery Date Checking App
This application is simple and is build to show developing skills on PHP, Laravel.

To run this application you need to have PHP installed and also you need to have registered PHP path to system variables. After env setup you need to simply go to the root of the application on command prompt and run the command as below
```bash
php artisan serve
```

## Database Seeding
To add random data to database simply run
```bash
php artisan db:seed
```


## Purpose of the application

This application is used to check the delivery date based on the order shipping number.

It's usage is simple you only need to enter the Shipping Number and if the order exists the application will return the Delivery Date.

## Author
[Shpetim Haxhiu](https://linkedin.com/in/shpetimhaxhiu)  
[Project Link](https://bitbucket.org/shpetim_haxhiu/laravel-restapi)
