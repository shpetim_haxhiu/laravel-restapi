<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Index page
Route::get('/', [
	'uses' => 'OrderController@index'
]);



// Create orders route
Route::get('/orders', [
    'uses' => 'OrderController@list'
	]
);



// Order based on shipping id
Route::get('/check', 'OrderController@check');


