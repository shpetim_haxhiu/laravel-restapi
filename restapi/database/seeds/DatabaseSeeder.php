<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// generate random timestamp
 		$int= mt_rand(1,time());

		//Format that timestamp into a readable date string.
		$randomDate = date("Y-m-d", $int);

		// Insert random data to DB
        DB::table('orders')->insert([
            'shipping_code' => Str::random(10),
            'delivery_date' => $randomDate,
            'created_at'	=> date("Y-m-d H:i:s"),
            'updated_at'	=> date("Y-m-d H:i:s"),
        ]);
    }
}