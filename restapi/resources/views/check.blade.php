<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        
        <!-- Bootstrap stylesheet -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Check your order delivery date
                </div>
                

                <form action="/check">
                    <div class="form-group row text-center">
                        <label for="shipping_code" class="col-sm-4 col-form-label">Enter shipping code</label>
                        <div class="col-sm-4">
                            <input  class="form-control" type="text" name="shipping_code" id="shipping_code" value="{{ $shipping_code }}">
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-primary btn-block" type="submit">Check it</button>
                        </div>
                    </div>
                </form>
                @if($error)
                    <p class="text-danger">{{$error}}</p>
                @else
                
                <p>Your order`s delivery date is: <span class="text-success font-weight-bold">{{ $order->delivery_date }}</span></p>

                @endif
            </div>
        </div>
    </body>
</html>
